from flask import current_app, Blueprint, request, session, g, redirect, url_for, abort, render_template, flash
import datetime, os
from werkzeug.utils import secure_filename
main_app = Blueprint('main_app', __name__, template_folder='templates')

@main_app.route('/')
def index(name=None):
    #print(current_app.config.get('HOST'))
    return render_template('index.html')
    
@main_app.route('/education')
def education():
    return render_template('education.html')
    
@main_app.route('/burials')
def burials():
    return render_template('burials.html')

@main_app.route('/donate')
def donate():
    return render_template('donate.html')

@main_app.route('/about')
def about():
    return render_template('about.html')

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1] == 'pdf'

# Route that will process the file upload
@main_app.route('/upload', methods=['POST'])
def upload():
    # Get the name of the uploaded file
    file = request.files['file']
    # Check if the file is one of the allowed types/extensions
    if file and allowed_file(file.filename):
        # Make the filename safe, remove unsupported chars
        filename = secure_filename(file.filename)
        # Move the file form the temporal folder to
        # the upload folder we setup
        file.save(os.path.join('/timetables', filename))
        # Redirect the user to the uploaded_file route, which
        # will basicaly show on the browser the uploaded file
        return index()
        # redirect(url_for('main_app.index') )

@main_app.route('/admin/timetable_upload')
def timetable_upload():
    return render_template('timetable_upload.html')
    
    