from flask import Flask
from views import main_app
from flask.ext.bower import Bower

app = Flask(__name__)
app.register_blueprint(main_app)

Bower(app)

if __name__ == '__main__':
    app.config.from_object('config')
    app.run(
        threaded=False,
        host=app.config.get('HOST'),
        port=app.config.get('PORT')
        )